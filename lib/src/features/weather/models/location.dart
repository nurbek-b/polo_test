class Location {
  final String state;
  final String region;
  final String name;

  Location({
    required this.state,
    required this.region,
    required this.name,
  });
}
