class GeoLocation {
  final double lat;
  final double long;

  GeoLocation({
    required this.lat,
    required this.long,
  });
}
