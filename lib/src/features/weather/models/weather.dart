import 'package:ptolemay_test/src/features/weather/models/location.dart';

class Weather {
  final double temperatureC;
  final double temperatureF;
  final Location state;

  Weather({
    required this.temperatureC,
    required this.temperatureF,
    required this.state,
  });
}
