import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ptolemay_test/src/features/weather/models/geo.dart';
import 'package:ptolemay_test/src/features/weather/models/weather.dart';
import 'package:ptolemay_test/src/features/weather/repositories/weather_repository.dart';

part 'weather_event.dart';

part 'weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final WeatherRepository weatherRepository;

  WeatherBloc({required this.weatherRepository}) : super(WeatherInitial()) {
    on<GetWeather>(_getWeather);
  }

  void getWeather() => add(GetWeather());

  Future<void> _getWeather(event, emit) async {
    try {
      final geoLocation = await _getUserLocation();
      final weather = await weatherRepository.getWeather(geoLocation);
      emit(WeatherLoaded(weather: weather));
    } catch (e) {
      log('weather get error: $e');
      emit(WeatherError(error: '$e'));
    }
  }

  Future<GeoLocation?> _getUserLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    log('permission: $permission');

    final locationData = await Geolocator.getCurrentPosition(
      timeLimit: const Duration(seconds: 20),
    );
    log('location Data: $locationData');
    return GeoLocation(
      lat: locationData.latitude,
      long: locationData.longitude,
    );
  }
}
