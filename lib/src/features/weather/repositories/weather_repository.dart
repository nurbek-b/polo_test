import 'package:ptolemay_test/src/features/weather/models/geo.dart';
import 'package:ptolemay_test/src/features/weather/models/location.dart';
import 'package:ptolemay_test/src/features/weather/models/weather.dart';
import 'package:ptolemay_test/src/services/client_service.dart';

class WeatherRepository {
  final ClientService clientService;

  WeatherRepository({
    required this.clientService,
  });

  Future<Weather> getWeather(GeoLocation? geoLocation) async {
    final response = await clientService
        .get('?q=${geoLocation?.long ?? .0}%2C${geoLocation?.lat ?? .0}');
    if (response.statusCode == 200) {
      final location = Location(
        state: response.data['location']['country'],
        region: response.data['location']['region'],
        name: response.data['location']['name'],
      );
      final tempC = response.data['current']['temp_c'];
      final tempF = response.data['current']['temp_f'];
      return Weather(temperatureC: tempC, temperatureF: tempF, state: location);
    } else {
      throw Exception("Failed to get weather");
    }
  }
}
