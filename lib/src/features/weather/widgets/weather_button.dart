import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ptolemay_test/src/features/weather/bloc/weather_bloc.dart';

class WeatherFetchButton extends StatelessWidget {
  const WeatherFetchButton({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        context.read<WeatherBloc>().getWeather();
      },
      child: const Icon(Icons.cloud),
    );
  }
}
