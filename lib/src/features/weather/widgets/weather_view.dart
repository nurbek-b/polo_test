import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ptolemay_test/src/features/weather/bloc/weather_bloc.dart';

class WeatherView extends StatelessWidget {
  const WeatherView({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeatherBloc, WeatherState>(
      builder: (context, state) {
        if (state is WeatherError) {
          return Center(
            child: Text(
              'Something went wrong! Error: ${state.error}',
              textAlign: TextAlign.center,
            ),
          );
        } else if (state is WeatherLoading) {
          return const CircularProgressIndicator();
        } else if (state is WeatherLoaded) {
          return Text(
              'Weather for ${state.weather.state.state}, ${state.weather.state.region}:'
              '${state.weather.temperatureC}C(${state.weather.temperatureF}F)');
        } else {
          return const Text('Press the icon to get your location and weather');
        }
      },
    );
  }
}
