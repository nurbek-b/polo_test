import 'package:flutter/material.dart';

class ThemeSwitchButton extends StatelessWidget {
  final VoidCallback? onPressed;

  const ThemeSwitchButton({required this.onPressed, super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: onPressed,
      child: const Icon(Icons.color_lens),
    );
  }
}
