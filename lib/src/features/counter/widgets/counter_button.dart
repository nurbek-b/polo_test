import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ptolemay_test/src/features/counter/bloc/counter_cubit.dart';
import 'package:ptolemay_test/src/shared/separator.dart';

class CounterButtons extends StatelessWidget {
  const CounterButtons({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CounterCubit, int>(
      builder: (context, state) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            if (state < 10)
              FloatingActionButton(
                onPressed: () => context.read<CounterCubit>().increment(),
                child: const Icon(Icons.add),
              )
            else
              const SizedBox(
                height: 56.0,
              ),
            const Separator.forColumn(20),
            if (state >= 1)
              FloatingActionButton(
                onPressed: () => context.read<CounterCubit>().decrement(),
                child: const Icon(Icons.remove),
              )
            else
              const SizedBox(
                height: 56.0,
              ),
          ],
        );
      },
    );
  }
}
