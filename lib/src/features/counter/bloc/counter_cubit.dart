import 'package:flutter_bloc/flutter_bloc.dart';

class CounterCubit extends Cubit<int> {
  final bool isDark;

  CounterCubit({
    required this.isDark,
  }) : super(0);

  void increment() => emit(state + (isDark ? 2 : 1));

  void decrement() => emit(state - (isDark ? 2 : 1));
}
