import 'package:flutter/material.dart';

ThemeData getTheme(bool dark) {
  if (dark) {
    return ThemeData.dark();
  } else {
    return ThemeData.light();
  }
}
