import 'package:dio/dio.dart';
import 'package:ptolemay_test/src/core/utils/app_constants.dart';

class ClientService {
  Dio dio = Dio();
  static const _baseUrl = AppConstants.weatherUrl;

  ClientService() {
    BaseOptions options = BaseOptions(
        baseUrl: _baseUrl,
        connectTimeout: const Duration(seconds: 10),
        receiveTimeout: const Duration(seconds: 10),
        headers: {
          'X-RapidAPI-Key': AppConstants.apiKey,
          'X-RapidAPI-Host': AppConstants.apiHost,
        });
    dio = Dio(options);
  }

  Future<Response> get(String path,
      {Map<String, dynamic>? queryParameters}) async {
    try {
      final response = await dio.get(
        _baseUrl + path,
        queryParameters: queryParameters,
      );

      return response;
    } on DioException catch (e) {
      throw Exception(e.message);
    }
  }

  Future<Response> post(String path, dynamic data, [Options? options]) async {
    try {
      final response = await dio.post(
        _baseUrl + path,
        data: data,
        options: options,
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> put(String path, dynamic data) async {
    try {
      final response = await dio.put(_baseUrl + path, data: data);
      return response;
    } on DioException catch (e) {
      throw Exception(e.message);
    }
  }

  Future<Response> delete(String path) async {
    try {
      final response = await dio.delete(_baseUrl + path);
      return response;
    } on DioException catch (e) {
      throw Exception(e);
    }
  }
}
