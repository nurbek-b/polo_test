import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ptolemay_test/src/core/injection.dart';
import 'package:ptolemay_test/src/features/counter/bloc/counter_cubit.dart';
import 'package:ptolemay_test/src/features/counter/widgets/counter_button.dart';
import 'package:ptolemay_test/src/features/theme/widgets/theme_switcher_button.dart';
import 'package:ptolemay_test/src/features/weather/bloc/weather_bloc.dart';
import 'package:ptolemay_test/src/features/weather/widgets/weather_button.dart';
import 'package:ptolemay_test/src/features/counter/widgets/counter_view.dart';
import 'package:ptolemay_test/src/features/weather/widgets/weather_view.dart';
import 'package:ptolemay_test/src/shared/separator.dart';
import 'package:ptolemay_test/src/theme/theme_transition.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isDark = false;
  GlobalKey key = GlobalKey();
  Offset position = Offset.zero;

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return DarkTransition(
      isDark: isDark,
      offset: Offset(mediaQuery.size.width - 20, mediaQuery.size.height - 20),
      duration: const Duration(milliseconds: 800),
      childBuilder: (context, int index) {
        return MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (_) => CounterCubit(isDark: isDark),
            ),
            BlocProvider(
              create: (_) => WeatherBloc(
                weatherRepository: sl(),
              ),
            ),
          ],
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Theme.of(context).colorScheme.inversePrimary,
              title: Text(widget.title),
            ),
            body: const Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  WeatherView(),
                  Separator.forColumn(20),
                  CounterView(),
                ],
              ),
            ),
            floatingActionButton: Padding(
              padding: const EdgeInsets.only(left: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      const WeatherFetchButton(),
                      const Separator.forColumn(20),
                      ThemeSwitchButton(onPressed: () {
                        setState(() {
                          isDark = !isDark;
                        });
                      }),
                    ],
                  ),
                  const CounterButtons(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
