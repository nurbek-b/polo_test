import 'dart:async';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ptolemay_test/src/core/injection.dart' as di;
import 'package:ptolemay_test/src/core/app.dart';
import 'package:ptolemay_test/src/core/app_config.dart';
import 'package:ptolemay_test/src/core/bloc_observer.dart';

Future<void> runPtolemayApp(AppConfig appConfig) async {
  Bloc.observer = const CounterObserver();
  await runZonedGuarded<Future<void>>(
    () async {
      await di.setUp(appConfig);

      runApp(
        const MyApp(),
      );
    },
    (error, stackTrace) => log(
      error.toString(),
      stackTrace: stackTrace,
    ),
  );
}
