import 'package:ptolemay_test/src/core/run_app.dart';

import 'app_config.dart';

Future<void> main() async {
  await runPtolemayApp(const AppConfig(appName: 'Weather Counter'));
}
