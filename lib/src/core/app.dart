import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ptolemay_test/src/core/injection.dart';
import 'package:ptolemay_test/src/features/counter/bloc/counter_cubit.dart';
import 'package:ptolemay_test/src/features/weather/bloc/weather_bloc.dart';
import 'package:ptolemay_test/src/screens/home/home_screen.dart';
import 'package:ptolemay_test/src/theme/theme_transition.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      title: 'Ptolemay Test',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(title: ''),
    );
  }
}
