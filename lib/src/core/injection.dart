import 'package:get_it/get_it.dart';
import 'package:ptolemay_test/src/core/app_config.dart';
import 'package:ptolemay_test/src/features/weather/repositories/weather_repository.dart';
import 'package:ptolemay_test/src/services/client_service.dart';

final sl = GetIt.instance;

Future<void> setUp(AppConfig appConfig) async {
  if (sl.isRegistered<AppConfig>()) return;

  sl
    ..registerSingleton(appConfig)
    ..registerSingleton<ClientService>(
      ClientService(),
    )
    ..registerSingleton<WeatherRepository>(
      WeatherRepository(
        clientService: sl(),
      ),
    );
}
