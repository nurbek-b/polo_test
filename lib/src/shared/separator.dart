import 'package:flutter/widgets.dart';

class Separator extends SizedBox {
  const Separator({
    double? width,
    double? height,
    Key? key,
  })  : assert((height == null) != (width == null),
            'Must set width or height, but not both or neither'),
        super(
          width: width ?? 0,
          height: height ?? 0,
          key: key,
        );

  const Separator.forRow(double width, {Key? key})
      : super(
          width: width,
          key: key,
        );

  const Separator.forColumn(double height, {Key? key})
      : super(
          height: height,
          key: key,
        );
}
